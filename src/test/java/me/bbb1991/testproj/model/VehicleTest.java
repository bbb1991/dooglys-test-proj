package me.bbb1991.testproj.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class VehicleTest {

    private Vehicle vehicle;

    @Before
    public void setUp() throws Exception {

        vehicle = new Vehicle(Direction.SOUTH);

    }

    @Test
    public void vehicleChangeDirection() {
        assertEquals(vehicle.getDirection(), Direction.SOUTH);

        vehicle.turnRight();
        assertEquals(vehicle.getDirection(), Direction.WEST);

        vehicle.turnRight();
        assertEquals(vehicle.getDirection(), Direction.NORTH);

        vehicle.turnRight();
        assertEquals(vehicle.getDirection(), Direction.EAST);

        vehicle.turnRight();
        assertEquals(vehicle.getDirection(), Direction.SOUTH);
    }
}