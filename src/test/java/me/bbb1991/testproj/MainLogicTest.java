package me.bbb1991.testproj;

import me.bbb1991.testproj.model.Command;
import me.bbb1991.testproj.model.Direction;
import me.bbb1991.testproj.model.Field;
import me.bbb1991.testproj.model.Vehicle;
import me.bbb1991.testproj.utils.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MainLogicTest {

    private MainLogic mainLogic;

    private Field field;

    @Before
    public void setUp() {
        mainLogic = new MainLogic();
        field = new Field(5, 5, "0 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 0");
    }

    @Test
    public void readMap() {
        Field field = mainLogic.readMap();
        assertNotNull(field);
    }

    @Test
    public void getCommands() {
        List<Pair<Command, String>> commands = mainLogic.getCommands();
        assertNotNull(commands);
    }

    @Test
    public void extractPoints() {
        Pair<Integer, Integer> pair = mainLogic.extractPoint("5 3");
        assertNotNull(pair);
        assertEquals(new Integer(5), pair.left());
        assertEquals(new Integer(3), pair.right());
    }

    @Test
    public void shouldNotMove() {
        Vehicle vehicle = new Vehicle(Direction.SOUTH, 0, 0);
        Pair<Integer, Integer> pair = Pair.of(2, 2);
        mainLogic.moveVehicle(vehicle, pair, field);
        assertNotEquals(vehicle.getX(), pair.getX().intValue());
        assertNotEquals(vehicle.getY(), pair.getY().intValue());
        assertEquals(vehicle.getX(), 0);
        assertEquals(vehicle.getY(), 0);
    }

    @Test
    public void shouldMove() {
        Vehicle vehicle = new Vehicle(Direction.SOUTH, 0, 0);
        Pair<Integer, Integer> pair = Pair.of(4, 4);
        mainLogic.moveVehicle(vehicle, pair, field);
        assertEquals(vehicle.getX(), pair.getX().intValue());
        assertEquals(vehicle.getY(), pair.getY().intValue());
    }
}