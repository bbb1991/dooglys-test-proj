package me.bbb1991.testproj;

import me.bbb1991.testproj.utils.ReadWriteUtils;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void readExistingFile() {
        List<String> lines = ReadWriteUtils.readFile("map.txt");
        assertNotNull(lines);
    }

}