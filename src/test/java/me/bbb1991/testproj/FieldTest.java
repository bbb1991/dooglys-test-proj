package me.bbb1991.testproj;

import me.bbb1991.testproj.model.Field;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FieldTest {

    private Field field;

    @Before
    public void setUp() throws Exception {
        field = new Field(5, 5, "0 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 0");
    }

    @Test
    public void obstructed() {
        boolean result = field.isObstructed(2, 2);
        assertTrue(result);
    }

    @Test
    public void negativeValue() {
        boolean result = field.isObstructed(0, 0);

        assertFalse(result);
    }

}