package me.bbb1991.testproj;

import me.bbb1991.testproj.model.Command;
import me.bbb1991.testproj.model.Direction;
import me.bbb1991.testproj.model.Field;
import me.bbb1991.testproj.model.Vehicle;
import me.bbb1991.testproj.utils.Pair;
import me.bbb1991.testproj.utils.ReadWriteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class MainLogic {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainLogic.class);

    public void moveVehicle(Vehicle vehicle, Pair<Integer, Integer> next, Field field) {

        if (field.isObstructed(next.getX(), next.getY())) {
            LOGGER.warn("Cannot move vehicle to the point : {}", next);
        } else {
            vehicle.setCurrentPoint(next);
        }
    }

    public void start() {
        Field field = readMap();
        List<Pair<Command, String>> commands = getCommands();
        Vehicle vehicle = new Vehicle();
        for (Pair<Command, String> pair : commands) {
            Command nextCommand = pair.left();
            LOGGER.info("Next command is: {}", nextCommand);
            switch (nextCommand) {
                case MOVE:
                    Pair<Integer, Integer> destinationPoint = this.extractPoint(pair.right());
                    moveVehicle(vehicle, destinationPoint, field);
                    break;
                case TURN:
                    Direction newDirection = Direction.valueOf(pair.right());
                    while (vehicle.getDirection() != newDirection) vehicle.turnRight();
                    break;
                default:
                    LOGGER.error("Command {} does not understood!", pair.left());
                    throw new IllegalArgumentException("Unknown command!");
            }
            LOGGER.info("Command {} executed. Current vehicle status: {}", nextCommand, vehicle);
        }

        ReadWriteUtils.writeFinalPosition(vehicle.getDirection(), vehicle.getX(), vehicle.getY());
    }

    /**
     * Чтение карты из файла
     *
     * @return
     */
    public Field readMap() {
        List<String> lines = ReadWriteUtils.readFile("map.txt");
        String[] data = lines.get(0).split("\\s", 3);
        int x = Integer.parseInt(data[0]);
        int y = Integer.parseInt(data[1]);
        String initialValue = data[2];

        return new Field(x, y, initialValue);
    }

    /**
     * Чтение списка команд из файла. команда состоит из 2 частей, сама команда {@link Command} и уточнение.
     * Если команда {@link Command#TURN} то это направление {@link Direction},
     * либо {@link Command#MOVE} с коорданатами в виде {@code x y}
     *
     * @return
     */
    public List<Pair<Command, String>> getCommands() {
        List<String> lines = ReadWriteUtils.readFile("commands.txt");
        return lines.stream()
                .map(e -> e.split("\\s", 2))
                .map(e -> Pair.of(Command.valueOf(e[0]), e[1]))
                .collect(Collectors.toList());
    }

    /**
     * Если команда была {@link Command#MOVE} то считываем координаты
     *
     * @param str
     * @return
     */
    public Pair<Integer, Integer> extractPoint(final String str) {
        int x = Integer.parseInt(str.split("\\s")[0]);
        int y = Integer.parseInt(str.split("\\s")[1]);

        return Pair.of(x, y);
    }
}
