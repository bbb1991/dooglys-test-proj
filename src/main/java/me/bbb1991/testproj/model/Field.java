package me.bbb1991.testproj.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Класс, описывающая карту
 */
public class Field {

    private static final Logger LOGGER = LoggerFactory.getLogger(Field.class);

    /**
     * ширина карты
     */
    private final int width;

    /**
     * длина карты
     */
    private final int height;

    /**
     * сама карта. может быть либо 0, то есть поле свободно либо 1 то есть есть препятсвие и вездеход не может
     * пересекать/останавливаться на данной клетке
     */
    private final boolean[][] map;

    /**
     * Конструктор для создания карты. Данные считываются из файла {@code map.txt}
     *
     * @param width  ширина карты
     * @param height длина карты
     * @param line данные для заполнения ячеек
     */
    public Field(int width, int height, String line) {
        LOGGER.debug("x: {}, y: {}, initial data is: {}", width, height, line);
        this.width = width;
        this.height = height;

        map = new boolean[width][height];
        fillMap(line);
        printArray();
    }

    private void fillMap(String line) {
        Boolean[] numbers = Arrays.stream(line.split("\\s"))
                .map(Integer::parseInt)
                .map(e -> e == 1)
                .toArray(Boolean[]::new);

        int c = 0;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                map[i][j] = numbers[c++];
            }
        }
    }

    /**
     * Проверка, не встретилась ли на пути препятсивие
     *
     * @return {@code true} если есть препятствие, иначе {@code false}
     */
    public boolean isObstructed(int x, int y) {
        if (x > -1 && y > -1) {
            return map[x][y];
        }
        return false;
    }

    /**
     * Печать карты на экран, чтобы визуально представлять с чем работаем
     */
    public void printArray() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.printf("%d ", map[i][j] ? 1 : 0);
            }
            System.out.println();
        }
    }

}
