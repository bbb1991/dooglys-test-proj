package me.bbb1991.testproj.model;

/**
 * Список возможных команд
 */
public enum Command {
    MOVE,
    TURN
}
