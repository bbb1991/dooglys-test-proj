package me.bbb1991.testproj.model;

/**
 * Список направлений, куда может повернуться вездеход
 */
public enum Direction {
    NORTH,
    WEST,
    SOUTH,
    EAST
}
