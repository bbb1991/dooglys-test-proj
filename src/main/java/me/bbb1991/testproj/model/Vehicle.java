package me.bbb1991.testproj.model;

import me.bbb1991.testproj.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Vehicle {

    /**
     * Координата, где находится вездеход
     */
    private Pair<Integer, Integer> currentPoint;

    private static final Logger LOGGER = LoggerFactory.getLogger(Vehicle.class);

    /**
     * Направление, в какую сторону смотрит вездеход
     */
    private Direction currentDirection;

    public Vehicle() {
        this(Direction.SOUTH, 0, 0);
    }

    public Vehicle(final Direction direction) {
        this(direction, 0, 0);
    }

    public Vehicle(final Direction direction, final int x, final int y) {
        this.currentDirection = direction;
        this.currentPoint = Pair.of(x, y);
    }

    /**
     * Сделать шаг по направлению
     */
    public void stepForward() {
        Pair<Integer, Integer> nextPoint = this.nextTile();
        LOGGER.debug("Step forward command. Old position: {}, new position: {}", currentPoint, nextPoint);
        this.currentPoint.newPair(nextPoint);
    }

    public Pair<Integer, Integer> nextTile() {
        switch (currentDirection) {
            case SOUTH:
                return Pair.of(currentPoint.getX(), currentPoint.getY() + 1);
            case WEST:
                return Pair.of(currentPoint.getX() - 1, currentPoint.getY());
            case NORTH:
                return Pair.of(currentPoint.getX(), currentPoint.getY() - 1);
            case EAST:
                return Pair.of(currentPoint.getX() + 1, currentPoint.getY());
            default:
                throw new IllegalArgumentException("This never should be happen!"); // TODO
        }

    }

    public Direction getDirection() {
        return currentDirection;
    }

    public int getX() {
        return currentPoint.left();
    }

    public int getY() {
        return currentPoint.right();
    }

    /**
     * Поворот направо
     */
    public void turnRight() {

        LOGGER.debug("Current direction: {}. Turning vehicle to right", this.currentDirection);

        if (currentDirection == Direction.NORTH) {
            currentDirection = Direction.EAST;
        } else if (currentDirection == Direction.EAST) {
            currentDirection = Direction.SOUTH;
        } else if (currentDirection == Direction.SOUTH) {
            currentDirection = Direction.WEST;
        } else if (currentDirection == Direction.WEST) {
            currentDirection = Direction.NORTH;
        }
    }

    /**
     * Поворот налево
     */
    public void turnLeft() {
        LOGGER.debug("Current direction: {}. Turning vehicle to left", this.currentDirection);

        if (currentDirection == Direction.NORTH) {
            currentDirection = Direction.WEST;
        } else if (currentDirection == Direction.WEST) {
            currentDirection = Direction.SOUTH;
        } else if (currentDirection == Direction.SOUTH) {
            currentDirection = Direction.EAST;
        } else if (currentDirection == Direction.EAST) {
            currentDirection = Direction.NORTH;
        }

    }

    public void setCurrentPoint(Pair<Integer, Integer> currentPoint) {
        this.currentPoint = currentPoint;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "currentPoint=" + currentPoint +
                ", currentDirection=" + currentDirection +
                '}';
    }
}
