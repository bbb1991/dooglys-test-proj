package me.bbb1991.testproj.utils;

public class Pair<X, Y> {

    private X x;
    private Y y;

    private Pair(final X x, final Y y) {
        this.x = x;
        this.y = y;
    }

    public X getX() {
        return x;
    }

    public Y getY() {
        return y;
    }

    public X left() {
        return x;
    }

    public Y right() {
        return y;
    }

    public void newPair(final Pair<X, Y> pair) {
        this.x = pair.left();
        this.y = pair.right();
    }

    public static <X, Y> Pair<X, Y> of(X x, Y y) {
        return new Pair<>(x, y);
    }

    @Override
    public String toString() {
        return String.format("x: %s, y: %s", x, y);
    }
}
