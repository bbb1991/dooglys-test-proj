package me.bbb1991.testproj.utils;

import me.bbb1991.testproj.model.Direction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadWriteUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadWriteUtils.class);

    /**
     * Так как известно, где расположены читаемые файлы, можно использовать только одну функцию для прочтения разных файлов
     *
     * @param filename название файла, которую необходимо считать
     * @return строки файла в виде {@link List<String>}
     */
    public static List<String> readFile(final String filename) {

        LOGGER.info("Trying to read file: {}", filename);

        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            return stream
                    .filter(e -> !e.trim().isEmpty())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error("Could not read file {}", filename, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Запись результатов в файл
     *
     * @param direction
     * @param x
     * @param y
     */
    public static void writeFinalPosition(final Direction direction, final int x, final int y) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("report.txt"))) {
            writer.write(String.format("%d %d %s%n", x, y, direction));
        } catch (IOException e) {
            LOGGER.error("Cannot write result to a file: report.txt", e);
            throw new RuntimeException(e);
        }
    }
}
